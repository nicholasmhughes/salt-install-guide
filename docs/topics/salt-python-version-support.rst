.. _salt-python-version-support:

===========================
Support for Python versions
===========================

Starting with Salt version 3006, the Salt Project aligns with the
`Python support lifecycle <https://devguide.python.org/#status-of-python-branches>`_,
which means that Salt supports the same versions of Python that are supported by
the `Python Software Foundation <https://www.python.org/psf/>`_.


Currently supported versions
----------------------------
Salt |release| requires Python 3.5 or higher.


Future supported versions
-------------------------
Salt 3006 requires Python 3.7 or higher.


Python 3.5 and 3.6 deprecation notice
-------------------------------------
Salt 3005 is the last release that supports Python versions 3.5 and 3.6.
In Salt release 3006, only Python versions 3.7 and higher will be supported.
