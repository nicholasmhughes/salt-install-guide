.. list-table::
  :widths: 10 10 40 40
  :header-rows: 1
  :class: windows-mac-download

  * - Arch
    - File type
    - Download install file
    - GPG

  * - AMD64
    - exe
    - |windows-amd64-exe-download|
    - |windows-amd64-exe-gpg|
